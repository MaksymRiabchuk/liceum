<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL); ?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Official site of Podolsk lyceum</title>
</head>
<body>
<div class="container">
    <?php
    require_once 'classes/MainMenu.php';
    \classes\MainMenu::OutputMenu('subjects');
    require_once 'db.php';
    require_once 'classes/Subject.php';
    $filter = $_GET;
    $subjects = \classes\subject::findAll($db,$filter);
    if (isset ($_GET['name'])){
        $name=$_GET['name'];
    }
    else{
        $name='';
    }
    if (isset($_GET['URL_book'])){
        $URL_book=$_GET['URL_book'];
    }
    else{
        $URL_book='';
    }
    if (isset($_GET['form'])){
        $form=$_GET['form'];
    }
    else{
        $form='';
    }
    ?>
    <style>
        th{
            background-color: #33FF74;
            color: white;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <h1>
                Список учнів
            </h1>
        </div>
    </div>
    <div class="row">
        <form>
            <div class="col-md-3"><label>Назва предмета</label><input type="text" name="name" class="form-control" value="<?=$name?>"></div>
            <div class="col-md-3"><label>Посиланння</label><input type="text" name="URL_book" class="form-control" value="<?=$URL_book?>"></div>
            <div class="col-md-3"><label>Клас</label><input type="text" name="form" class="form-control" value="<?=$form?>"></div>
            <div class="col-md-3">
                <div class="form-group" style="padding-top: 24px;">
                    <input class="btn btn-primary pull-right" type="submit" name="submit" value="Вибрати">
                </div>
            </div>
            <div class="clearfix" ></div>
        </form>
    </div>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>Назва предмета</th>
            <th>Редагування посилання</th>
            <th>Клас</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($subjects as $subject): ?>
            <tr>
                <td>
                    <?= $subject->name?>
                </td>
                <td>
                    <a target="_blank" href="<?= $subject->URL_book ?>">download</a>
                </td>
                <td>
                    <?= $subject->form?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>
