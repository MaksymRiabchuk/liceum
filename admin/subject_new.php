<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'subjects';
require_once '../classes/Subject.php';
require_once '../db.php';
$subject = new  \classes\Subject($db);

if (isset ($_GET['submit'])) {
    echo 'Збереження даних';
    $subject->loadFromGet($_GET);
    $subject->save();
    header("Location: http://liceum/admin/subjects.php");
}
?>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    require_once '../classes/AdminMenu.php';
    \classes\AdminMenu::OutputMenu('subjects');
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список учнів</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-info">
                <div class="panel panel-default">
                    <div class="panel-heading">Добавлення предмета</div>
                    <div class="panel-body">
                        <div class="col-md-2">
                            <form role="form">
                                <div class="form-group">
                                    <label>Назва предмета </label>
                                    <input name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Посилання на підручник</label>
                                    <input name="URL_book" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Клас</label>
                                    <input name="form" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input name="submit" class="btn btn-primary" type="submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>