<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'mark';
require_once '../classes/Mark.php';
require_once '../db.php';
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    require_once '../classes/AdminMenu.php';
    \classes\AdminMenu::OutputMenu('marks');
    $filter = $_GET;
    /* @var $db mysqli */
    $marks = \classes\Mark::findAll($db, $filter);
    if (isset ($_GET['student_name'])) {
        $student_name = $_GET['student_name'];
    } else {
        $student_name = '';
    }
    if (isset($_GET['subject_name'])) {
        $subject_name = $_GET['subject_name'];
    } else {
        $subject_name = '';
    }
    if (isset($_GET['teacher_name'])) {
        $teacher_name = $_GET['teacher_name'];
    } else {
        $teacher_name = '';
    }
    if (isset($_GET['mark'])) {
        $mark = $_GET['mark'];
    } else {
        $mark = '';
    }
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список оцінок</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Список оцінок </h3>
                    <div class="btn btn-info  pull-right">
                        <a href="mark_new.php">Добавити</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form>
                            <div class="col-md-2"><label>Ім`я учня</label><input type="text" name="student_name"
                                                                                 class="form-control"
                                                                                 value="<?= $student_name ?>"></div>
                            <div class="col-md-2"><label>Предмет</label><input type="text" name="subject_name"
                                                                               class="form-control"
                                                                               value="<?= $subject_name ?>"></div>
                            <div class="col-md-2"><label>Вчитель</label><input type="text" name="teacher_name"
                                                                               class="form-control"
                                                                               value="<?= $teacher_name ?>"></div>
                            <div class="col-md-2"><label>Оцінка</label><input type="text" name="mark"
                                                                              class="form-control" value="<?= $mark ?>">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" style="padding-top: 24px;">
                                    <input class="btn btn-primary pull-right" type="submit" name="submit"
                                           value="Вибрати">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class='table table-bordered table-hover'>
                                <thead>
                                <tr>
                                    <th>Студент</th>
                                    <th>Вчитель</th>
                                    <th>Предмет</th>
                                    <th>Оцінка</th>
                                    <th>Дії</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php /* @var $mark \classes\Mark */ ?>
                                <?php foreach ($marks as $mark): ?>
                                    <tr>
                                        <td> <?= $mark->student_name . ' ' . $mark->student_surname ?></td>
                                        <td> <?= $mark->teacher_name ?></td>
                                        <td> <?= $mark->subject_name ?></td>
                                        <td> <?= $mark->mark ?></td>

                                        <td>
                                            <a href="/admin/mark_edit.php?id=<?= $mark->id ?>"><i
                                                        class="glyphicon glyphicon-pencil"></i></a>
                                            <a href="/admin/mark_delete.php?id=<?= $mark->id ?>"
                                               onclick="return confirm('Ви дійсно хочете видалити');"><i
                                                        class="glyphicon glyphicon-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>


