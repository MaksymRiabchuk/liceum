<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'teachers';
require_once '../classes/Teacher.php';
require_once '../db.php';
$teacher = new  \classes\Teacher($db);

if (isset ($_GET['submit'])) {
    echo 'Збереження даних';
    $teacher->loadFromGet($_GET);
    $teacher->save();
    header("Location: http://liceum/admin/teachers.php");
}
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
<?php
require_once '../classes/AdminMenu.php';
\classes\AdminMenu::OutputMenu('teachers');
?>
<div class="page-content">
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
        <li class="xn-icon-button pull-right">
            <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                        class="fa fa-sign-out"></span></a>
        </li>
    </ul>
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active">Список вчителів</li>
    </ul>
    <div class="page-content-wrap">
        <div class="panel panel-default">
            <div class="panel-heading"><h2 class="panel-title">Добавлення учителя</h2></div>
            <div class="panel-body">
                <form class="row">
                    <div class="col-md-2">
                        <form role="form">
                            <div class="form-group">
                                <label>Імя учителя </label>
                                <input name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Предмет учителя</label>
                                <input name="subject" class="form-control">
                            </div>
                            <div class="form-group">
                                <input name="submit" class="btn btn-primary" type="submit">
                            </div>
                        </form>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
