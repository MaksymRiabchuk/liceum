<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'marks';
require_once '../classes/Mark.php';
require_once '../db.php';
require_once '../classes/AdminMenu.php';
require_once '../classes/Student.php';
require_once '../classes/Teacher.php';
require_once '../classes/Subject.php';

/* @var $db mysqli */
$mark = new \classes\Mark($db);
if (!isset($_GET ['id'])) {
    $id = 1;
} else {
    $id = $_GET['id'];
}
$mark->findOne($id);

if (isset ($_GET['submit'])) {
    echo 'Збереження даних';
    $mark->loadFromGet($_GET);
    $mark->save();
    header("Location: http://liceum/admin/marks.php");
}
?>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    \classes\AdminMenu::OutputMenu('marks');
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список оцінок</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-default">
                <div class="panel-heading">Редагування оцінки</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <form role="form">
                                <input name="id" value="<?= $mark->id ?>" type="hidden">
                                <div class="form-group">
                                    <label>Ім'я учня </label>
                                    <select name="student_id" class="form-control">
                                        <?php foreach (\classes\Student::getStudentList($db) as $key => $item): ?>
                                            <?php $selected=$mark->student_id==$key ?'selected' : '' ?>
                                            <option value="<?= $key ?>" <?=$selected?>> <?= $item ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Ім'я вчителя</label>
                                    <select name="teacher_id" class="form-control">
                                    <?php foreach (\classes\Teacher::getTeacherList($db) as $key => $item): ?>
                                        <?php $selected=$mark->teacher_id==$key ?'selected' : '' ?>
                                        <option value="<?= $key ?>" <?=$selected?>> <?= $item ?></option>
                                    <?php endforeach; ?>]
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Предмет</label>
                                    <select name="subject_id" class="form-control">
                                    <?php foreach (\classes\Subject::getSubjectList($db) as $key => $item): ?>
                                        <?php $selected=$mark->subject_id==$key ?'selected' : '' ?>
                                        <option value="<?= $key ?>" <?=$selected?>> <?= $item ?></option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Оцінка</label>
                                    <input name="mark" class="form-control" value="<?=$mark->mark?>">
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-primary" type="submit" name="submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

