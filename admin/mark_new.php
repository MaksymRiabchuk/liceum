<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'mark';
require_once '../classes/Mark.php';
require_once '../db.php';
require_once '../classes/AdminMenu.php';
require_once '../classes/Student.php';
require_once '../classes/Teacher.php';
require_once '../classes/Subject.php';
require_once '../classes/DropDownHelper.php';

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    \classes\AdminMenu::OutputMenu('mark');
    /* @var $db mysqli */
    $mark = new \classes\Mark($db);
    if (isset ($_GET['submit'])) {
        echo 'Збереження даних';
        $mark->loadFromGet($_GET);
        $mark->save();
        header("Location: http://liceum/admin/marks.php");

    }
    if (isset ($_GET['name'])) {
    $name = $_GET['name'];
    } else {
    $name = '';
    }
    $filter = $_GET;
    $marks = \classes\Mark::findAll($db, $filter);
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список оцінок</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Добавлення Оцінки</h2>
                    <div class="btn btn-info  pull-right">
                        <a href="mark_new.php">Добавити</a>
                    </div>
                </div>
                <div class="page-content-wrap">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <form role="form">
                                        <div class="form-group">
                                            <label>Студент</label>
                                                <?=\classes\DropDownHelper::widget('student_id',\classes\Student::getStudentList($db))?>
                                        </div>
                                        <div class="form-group">
                                            <label>Учитель</label>
                                            <?=\classes\DropDownHelper::widget('teacher_id',\classes\Teacher::getTeacherList($db))?>
                                        </div>
                                        <div class="form-group">
                                            <label>Предмет</label>
                                            <?=\classes\DropDownHelper::widget('subject_id',\classes\Subject::getSubjectList($db))?>
                                        </div>
                                        <div class="form-group">
                                            <label>Оцінка</label>
                                            <input name="mark" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <input class="btn btn-primary" type="submit" name="submit">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>