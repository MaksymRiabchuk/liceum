<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'students';
require_once '../classes/Student.php';
require_once '../db.php';
$student = new \classes\Student($db);
if (!isset($_GET ['id'])) {
    $id = 1;
} else {
    $id = $_GET['id'];
}
$student->findOne($id);

if (isset ($_GET['submit'])) {
    echo 'Збереження даних';
    $student->loadFromGet($_GET);
    $student->save();
    header("Location: http://liceum/admin/students.php");
}
?>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    require_once '../classes/AdminMenu.php';
    \classes\AdminMenu::OutputMenu('students');
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список учнів</li>
        </ul>
        <div class="page-content-wrap"
        <div class="panel panel-default">
            <div class="panel-heading">Редагування учня</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-2">
                        <form role="form">
                            <input name="id" value="<?= $student->id ?>" type="hidden">
                            <div class="form-group">
                                <label>Ім'я учня </label>
                                <input name="name" class="form-control" value="<?= $student->name ?>">
                            </div>
                            <div class="form-group">
                                <label>Прізвище учня</label>
                                <input name="surname" class="form-control" value="<?= $student->surname ?>">
                            </div>
                            <div class="form-group">
                                <label>Клас учня</label>
                                <input name="form" class="form-control" value="<?= $student->form ?>">
                            </div>
                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" name="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
