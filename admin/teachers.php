<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'teachers';
require_once '../classes/Teacher.php';
require_once '../db.php';
if (isset ($_GET['name'])){
    $name=$_GET['name'];
}
else{
    $name='';
}
if (isset($_GET['subject'])){
    $subject=$_GET['subject'];
}
else{
    $subject='';
}
$filter = $_GET;
/* @var $db mysqli */
$teachers = \classes\teacher::findAll($db, $filter);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">
    <?php
    require_once '../classes/AdminMenu.php';
    \classes\AdminMenu::OutputMenu('teachers');
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список учителів</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Список вчителів </h3>
                    <div class="btn btn-info  pull-right">
                        <a href="teachers_new.php">Добавити</a>
                    </div>
                </div>
                <div class="panel panel-body">
                    <style>
                        th{
                            background-color: #33FF74;
                            color: white;
                        }
                    </style>
                    <div class="row">
                        <form>
                            <div class="col-md-3"><label>Учитель</label><input type="text" name="name" class="form-control" value="<?=$name?>"></div>
                            <div class="col-md-3"><label>Предмет</label><input type="text" name="subject" class="form-control" value="<?=$subject?>"></div>
                            <div class="col-md-6">
                                <div class="form-group" style="padding-top: 24px;">
                                    <input class="btn btn-primary pull-right" type="submit" name="submit" value="Вибрати">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <table class='table table-bordered'>
                        <thead>
                        <tr>
                            <th>Ім'я</th>
                            <th>Предмет</th>
                            <th>Дії</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($teachers as $teacher): ?>
                            <tr>
                                <td> <?= $teacher->name ?></td>
                                <td> <?= $teacher->subject ?></td>
                                <td>
                                    <a href="/admin/teachers_edit.php?id=<?= $teacher->id ?>"><i
                                                class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="/admin/teachers_delete.php?id=<?= $teacher->id ?>"
                                       onclick="return confirm('Ви дійсно хочете видалити');"><i
                                                class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>