<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once '../classes/Mark.php';
require_once '../db.php';
/* @var $db mysqli */
$mark = new \classes\Mark($db);
if (isset($_GET ['id'])) {
    $id = $_GET['id'];
    $mark->delete($id);
}
header("Location: http://liceum/admin/marks.php"); ?>
