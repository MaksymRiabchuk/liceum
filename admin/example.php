<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once "../db.php";
$active_menu = 'students';
require_once '../classes/Student.php';
?>

<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>

<div class="panel panel-info">
    <div class="panel-heading">Список учнів</div>
    <div class="panel-body">
        <div class="btn btn-success">
            <a href="example_new.php">Добавити</a>
        </div>
        <table class='table table-bordered'>
            <thead>
            <tr>
                <th>Ім'я</th>
                <th>Прізвище</th>
                <th>Клас</th>
                <th>Дії</th>
            </tr>
            </thead>
            <tbody>
            <?php $students = \classes\Student::findAll($db); ?>
            <?php foreach ($students as $student) : ?>
                <tr>
                    <td><?= $student->name ?></td>
                    <td><?= $student->surname ?></td>
                    <td><?= $student->form ?></td>
                    <td>
                        <a href="/admin/example_edit.php?id=<?= $student->id ?>"><i
                                    class="glyphicon glyphicon-pencil"></i></a>
                        <a href="/admin/example_delete.php?id=<?= $student->id ?>"><i
                                    class="glyphicon glyphicon-trash"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


<div>
    <p class="button">
    </p>
</div>


</body>


