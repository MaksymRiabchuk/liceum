<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once '../classes/Teacher.php';
require_once '../db.php';
$teacher=new \classes\Teacher($db);
if (isset($_GET ['id'])){
    $id=$_GET['id'];
    $teacher->delete($id);
}
header("Location: http://liceum/admin/teachers.php");?>
