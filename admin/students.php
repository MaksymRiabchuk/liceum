<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'students';
require_once '../classes/Student.php';
require_once '../db.php';
$students = \classes\Student::findAll($db);
$formArray = [
    8 => '8 клас',
    9 => '9 клас',
    10 => '10 клас',
    11 => '11 клас',
]
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    require_once '../classes/AdminMenu.php';
    \classes\AdminMenu::OutputMenu('students');
    require_once '../classes/StudentsFilter.php';
    $filterValue=\classes\StudentsFilter::filter($_GET);
    $filter = $_GET;
    /* @var $db mysqli */
    $students = \classes\student::findAll($db, $filter);
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список учнів</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Список учнів </h3>
                    <div class="btn btn-info  pull-right">
                        <a href="students_new.php">Добавити</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form>
                            <div class="col-md-3"><label>Ім`я</label><input type="text" name="name" class="form-control"
                                                                            value="<?= $filterValue['name'] ?>"></div>
                            <div class="col-md-3"><label>Прізвище</label><input type="text" name="surname"
                                                                                class="form-control"
                                                                                value="<?= $filterValue['surname'] ?>"></div>
                            <div class="col-md-3"><label>Клас</label><input type="text" name="form" class="form-control"
                                                                            value="<?= $filterValue['form'] ?>"></div>
                            <div class="col-md-3">
                                <div class="form-group" style="padding-top: 24px;">
                                    <input class="btn btn-primary pull-right" type="submit" name="submit"
                                           value="Вибрати">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <table class='table table-bordered table-hover'>
                        <thead>
                        <tr>
                            <th>Ім'я</th>
                            <th>Прізвище</th>
                            <th>Клас</th>
                            <th>Дії</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($students as $student): ?>
                            <tr>
                                <td> <?= $student->name ?></td>
                                <td> <?= $student->surname ?></td>
                                <td>  <?=  (isset($formArray[$student->form])) ? $formArray[$student->form] :'Недопустиме значення' ?></td>
                                <td>
                                    <a href="/admin/students_edit.php?id=<?= $student->id ?>"><i
                                                class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="/admin/students_delete.php?id=<?= $student->id ?>"
                                       onclick="return confirm('Ви дійсно хочете видалити');"><i
                                                class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>


