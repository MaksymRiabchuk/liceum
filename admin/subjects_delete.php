<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once '../classes/Subject.php';
require_once '../db.php';
$subject=new \classes\Subject($db);
if (isset($_GET ['id'])){
    $id=$_GET['id'];
    $subject->delete($id);
}
header("Location: http://liceum/admin/subjects.php");?>
