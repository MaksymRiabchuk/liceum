<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

$active_menu = 'subjects';
require_once '../classes/Subject.php';
require_once '../db.php';
$filter = $_GET;
$subjects = \classes\subject::findAll($db,$filter);
if (isset ($_GET['name'])){
    $name=$_GET['name'];
}
else{
    $name='';
}
if (isset($_GET['URL_book'])){
    $URL_book=$_GET['URL_book'];
}
else{
    $URL_book='';
}
if (isset($_GET['form'])){
    $form=$_GET['form'];
}
else{
    $form='';
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="css/theme-default.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="page-container">
    <?php
    require_once '../classes/AdminMenu.php';
    \classes\AdminMenu::OutputMenu('subjects');
    ?>
    <div class="page-content">
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <li class="xn-icon-button pull-right">
                <a href="../index.php" class="mb-control" data-box="#mb-signout"><span
                            class="fa fa-sign-out"></span></a>
            </li>
        </ul>
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Список предметів</li>
        </ul>
        <div class="page-content-wrap">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Список предметів </h3>
                    <div class="btn btn-info  pull-right">
                        <a href="subject_new.php">Добавити</a>
                    </div>
                </div>
                <div class="panel panel-body">
                    <div class="row">
                        <form>
                            <div class="col-md-3"><label>Назва предмета</label><input type="text" name="name" class="form-control" value="<?=$name?>"></div>
                            <div class="col-md-3"><label>Посилання</label><input type="text" name="subject" class="form-control" value="<?=$URL_book?>"></div>
                            <div class="col-md-3"><label>Клас</label><input type="text" name="subject" class="form-control" value="<?=$form?>"></div>
                            <div class="col-md-3">
                                <div class="form-group" style="padding-top: 24px;">
                                    <input class="btn btn-primary pull-right" type="submit" name="submit" value="Вибрати">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                    <table class='table table-bordered'>
                        <thead>
                        <tr>
                            <th>Назва предмета</th>
                            <th>Посилання</th>
                            <th>Клас</th>
                            <th>Дії</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($subjects as $subject): ?>
                            <tr>
                                <td><?= $subject->name ?></td>
                                <td>
                                    <a target="_blank" href="<?= $subject->URL_book ?>">download</a>
                                </td>
                                <td><?= $subject->form ?></td>
                                <td>
                                    <a href="/admin/subject_edit.php?id=<?= $subject->id ?>"><i
                                                class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="/admin/subjects_delete.php?id=<?= $subject->id ?>"
                                       onclick="return confirm('Ви дійсно хочете видалити');">
                                        <i class="glyphicon glyphicon-trash"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

