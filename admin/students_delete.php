<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once '../classes/Student.php';
require_once '../db.php';
$student = new \classes\Student($db);
if (isset($_GET ['id'])) {
    $id = $_GET['id'];
    $student->delete($id);
}
header("Location: http://liceum/admin/students.php"); ?>