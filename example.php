<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

require_once "db.php";
$active_menu = 'students';
require_once 'classes/Student.php';
?>

<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="container">
    <h1>
        Список учнів
    </h1>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>Ім`я</th>
            <th>Прізвище</th>
            <th>Клас</th>
        </tr>
        </thead>
        <tbody>
        <?php
        /* @var $db mysqli */
        $students = classes\Student::findAll($db);
        ?>
        <?php foreach ($students as $student): ?>
            <tr>
                <td>
                    <?= $student->name ?>
                </td>
                <td>
                    <?= $student->surname ?>
                </td>
                <td>
                    <?= $student->form ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<footer>
    2021 р.
</footer>
</body>
</html>
