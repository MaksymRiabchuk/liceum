<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL); ?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="container">
    <?php
    require_once 'classes/MainMenu.php';
    \classes\MainMenu::OutputMenu('students');
    require_once 'db.php';
    require_once 'classes/Student.php';
    if (isset ($_GET['name'])){
        $name=$_GET['name'];
    }
    else{
        $name='';
    }
    if (isset($_GET['surname'])){
        $surname=$_GET['surname'];
    }
    else{
        $surname='';
    }
    if (isset($_GET['form'])){
        $form=$_GET['form'];
    }
    else{
        $form='';
    }
    $filter = $_GET;
    /* @var $db mysqli */
    $students = \classes\student::findAll($db, $filter);
    ?> <style>
        th{
            background-color: #33FF74;
            color: white;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <h1>
                Список учнів
            </h1>
        </div>
    </div>
    <div class="row">
        <form>
            <div class="col-md-3"><label>Ім`я</label><input type="text" name="name" class="form-control" value="<?= $name?>"></div>
            <div class="col-md-3"><label>Прізвище</label><input type="text" name="surname" class="form-control"<?= $surname?>></div>
            <div class="col-md-3"><label>Клас</label><input type="text" name="form" class="form-control"<?=$form?>></div>
            <div class="col-md-3">
                <div class="form-group" style="padding-top: 24px;">
                    <input class="btn btn-primary pull-right" type="submit" name="submit" value="Вибрати">
                </div>
            </div>
            <div class="clearfix" ></div>
        </form>
    </div>

    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>Ім`я</th>
            <th>Прізвище</th>
            <th>Клас</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($students as $student): ?>
            <tr>
                <td>
                    <?= $student->name ?>
                </td>
                <td>
                    <?= $student->surname ?>
                </td>
                <td>
                    <?= $student->form ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>
