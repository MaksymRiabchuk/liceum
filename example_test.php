<html lang="ru">
<head>
    <meta charset="UTF-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>


<div class="container">
    <?php
require_once 'classes/MainMenu.php';
\classes\MainMenu::OutputMenu('example_tests');
?>
    <h1>
        Приклади тесту
    </h1>
    <?php
    require_once 'db.php';

    $stmt = $db->query('SELECT * from teachers');
    echo "<table class='table table-bordered'>";
    echo "<thead><tr><th>Прізвище та ім`я</th><th>Предмет(и)</th></tr></thead>";
    echo "<tbody>";
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr><td>" . $row['name'] . '</td><td>' . $row['subject'] . "</td></tr>"; //и т.д...
    }
    echo "</tbody>";
    echo "</table>"
    ?>
</div>
</body>
</html>
