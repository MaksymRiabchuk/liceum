<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL); ?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="container">
    <?php
    require_once 'classes/MainMenu.php';
    \classes\MainMenu::OutputMenu('index');
    ?>
    <h1 class="main-info">
        Вінницький науково-технічний ліцей для обдарованой молоді
    </h1>
    <p class="main-info">
        Ліцей розпочав приймати дітей лише у 2018 році.І на початку ми планували приймати дітей 10-11 класу.
        Але потім ми вирішили приймати з 8 по 11 клас.В подільському ліцею буде більше олімпіад,конкурсів, велике
        пространство для захоплення учнів.
        Також класи будуть ділитись на ІТ,М та ФХ.Тобто для IT будуть профільними математика, інформатика, українська
        мова та фізика
        Для ФХ математика українська мова фізика та хімія. Для М поглиблена математика, фізика та українська мова.
    </p>
    <div class="logo">
        <div>

        </div>
    </div>
</div>
</body>
</html>
