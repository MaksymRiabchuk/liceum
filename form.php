<html lang="ru">
<?php
    $form = isset($_GET['form']) ? $_GET['form'] : 8;
?>
<head>
    <meta charset="UTF-8">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="main-menu">
    <ul class="nav nav-pills">
        <li role="presentation" <?php if($form=="8") {echo 'class="active"';}?>><a href="/form.php?form=8">8-клас</a></li>
        <li role="presentation" <?php if($form=="9") {echo 'class="active"';}?>><a href="/form.php?form=9">9-клас</a></li>
        <li role="presentation" <?php if($form=="10") {echo 'class="active"';}?>><a href="/form.php?form=10">10-клас</a></li>
        <li role="presentation" <?php if($form=="11") {echo 'class="active"';}?>><a href="/form.php?form=11">11-клас</a></li>
    </ul>
</div>

<div class="main">
    <h1>
        Список Предметів
    </h1>
    <?php
    require_once 'db.php';
    $stmt = $db->query('SELECT * from subjects where form='.$form);
    echo "<table class='table table-bordered'>";
    echo "<thead><tr><th>Предмет</th><th>Скачати</th></tr></thead>";
    echo "<tbody>";
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr><td>" . $row['name'] . '</td><td><a href="' . $row['URL_book'] . '">download</a></td></tr>'; //и т.д...
    }
    echo "</tbody>";
    echo "</table>"
    ?>
    <p>
        раладаку
    </p>
</div>
</body>
</html>
