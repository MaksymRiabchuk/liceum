<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL); ?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="container">
    <?php
    require_once 'classes/MainMenu.php';
    \classes\MainMenu::OutputMenu('marks');
    require_once 'db.php';
    require_once 'classes/Mark.php';
    if (isset ($_GET['student_name'])){
        $student_name=$_GET['student_name'];
    }
    else{
        $student_name='';
    }
    if (isset($_GET['subject_name'])){
        $subject_name=$_GET['subject_name'];
    }
    else{
        $subject_name='';
    }
    if (isset($_GET['teacher_name'])){
        $teacher_name=$_GET['teacher_name'];
    }
    else{
        $teacher_name='';
    }
    if (isset($_GET['mark'])){
        $mark=$_GET['mark'];
    }
    else{
        $mark='';
    }
    $filter = $_GET;
    /* @var $db mysqli */
    $marks = \classes\Mark::findAll($db, $filter);
    ?> <style>
        th{
            background-color: #33FF74;
            color: white;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <h1>
                Список оцінок
            </h1>
        </div>
    </div>
    <div class="row">
        <form>
            <div class="col-md-2"><label>Ім`я учня</label><input type="text" name="student_name" class="form-control" value="<?= $student_name?>"></div>
            <div class="col-md-2"><label>Предмет</label><input type="text" name="subject_name" class="form-control" value="<?= $subject_name?>"></div>
            <div class="col-md-2"><label>Вчитель</label><input type="text" name="teacher_name" class="form-control" value="<?=$teacher_name?>"></div>
            <div class="col-md-2"><label>Оцінка</label><input type="text" name="mark" class="form-control" value="<?=$mark?>"></div>
            <div class="col-md-4">
                <div class="form-group" style="padding-top: 24px;">
                    <input class="btn btn-primary pull-right" type="submit" name="submit" value="Вибрати">
                </div>
            </div>
            <div class="clearfix" ></div>
        </form>
    </div>

    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>Ім`я учня</th>
            <th>Предмет</th>
            <th>Учитель</th>
            <th>Оцінка</th>
        </tr>
        </thead>
        <tbody>
        <?php /* @var $mark \classes\Mark */ ?>
        <?php foreach ($marks as $mark): ?>
            <tr>
                <td> <?= $mark->student_name . ' ' . $mark->student_surname ?></td>
                <td> <?= $mark->subject_name ?></td>
                <td> <?= $mark->teacher_name ?></td>
                <td> <?= $mark->mark ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>
