<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL); ?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Oficcial site of Podylskiy liceum</title>
</head>
<body>
<div class="container">
    <?php
    require_once 'classes/MainMenu.php';
    \classes\MainMenu::OutputMenu('teachers');
    require_once 'db.php';
    require_once 'classes/Teacher.php';
    if (isset ($_GET['name'])){
        $name=$_GET['name'];
    }
    else{
        $name='';
    }
    if (isset($_GET['subject'])){
        $subject=$_GET['subject'];
    }
    else{
        $subject='';
    }
    $filter = $_GET;
    /* @var $db mysqli */
    $teachers = \classes\teacher::findAll($db, $filter);
    ?>
    <style>
        th{
            background-color: #33FF74;
            color: white;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            <h1>
                Список вчителів
            </h1>
        </div>
    </div>
    <div class="row">
        <form>
            <div class="col-md-3"><label>Учитель</label><input type="text" name="name" class="form-control" value="<?=$name?>"></div>
            <div class="col-md-3"><label>Предмет</label><input type="text" name="subject" class="form-control" value="<?=$subject?>"></div>
            <div class="col-md-6">
                <div class="form-group" style="padding-top: 24px;">
                    <input class="btn btn-primary pull-right" type="submit" name="submit" value="Вибрати">
                </div>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
    <table class='table table-bordered'>
        <thead>
        <tr>
            <th>Учитель</th>
            <th>Предмет</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($teachers as $teacher): ?>
            <tr>
                <td>
                    <?= $teacher->name ?>
                </td>
                <td>
                    <?= $teacher->subject ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>
