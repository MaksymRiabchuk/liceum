<?php


namespace classes;

use mysqli;

class Teacher
{
    public $id;
    public $name;
    public $subject;

    /* @var $_db mysqli */
    private $_db;

    public function __construct($db)
    {
        $this->_db = $db;
    }

    public function findOne($id)
    {
        /* @var $this- >_db mysqli */
        $result = $this->_db->query("SELECT * FROM teachers WHERE id=".$id);

        if (!$result) {
            printf("Ошибка: %s\n", $this->_db->error);
            return false;
        }

        $row = $result->fetch_assoc();
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->subject = $row['subject'];
    }


    public static function findAll($db, $filter = []): array
    {
        /* @var $db mysqli */

        $sql = "SELECT * FROM teachers";
        if (isset($filter['name'])) {
            $sql .= ' where name like "%' . $filter['name'] . '%"';
        }
        if (isset($filter['subject'])) {
            $sql .= ' and subject like "%' . $filter['subject'] . '%"';
        }
        $result = $db->query($sql);
        if (!$result) {
            printf("Ошибка: %s\n", $db->error);
            return false;
        }

        $row = $result->fetch_all(MYSQLI_ASSOC);
        $teachers = [];
        foreach ($row as $item) {
            $teacher = new self($db);
            $teacher->id = $item['id'];
            $teacher->name = $item['name'];
            $teacher->subject = $item['subject'];
            $teachers[] = $teacher;
        }

        return $teachers;
    }

    public function loadFromGet($data)
    {
        if (isset($data['id'])) {
            $this->id = intval($data['id']);
        }
        if (isset($data['name'])) {
            $this->name = htmlspecialchars($data['name']);
        }
        if (isset($data['subject'])) {
            $this->subject = htmlspecialchars($data['subject']);
        }
    }


    public function save()
    {
        if ($this->id == null) {
            $result = $this->_db->prepare("INSERT INTO teachers VALUES (null,?,?)");
        } else {
            $result = $this->_db->prepare("UPDATE teachers SET name=?, subject=? WHERE id =" . $this->id);
        }
        $result->bind_param("ss", $this->name, $this->subject);
        $result->execute();
        $result->close();
    }

    public function delete($id)
    {
        $result = $this->_db->query("DELETE FROM teachers WHERE id = " . $id);
    }
    public static function getTeacherList($db){
        $array=self::findAll($db);
        $teacherList=[];
        /* @var $item \classes\Teacher*/
        foreach ($array as $item){
            $teacherList[$item->id]=$item->name;
        }
        return $teacherList;
    }

}