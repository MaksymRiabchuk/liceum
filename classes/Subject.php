<?php


namespace classes;

use mysqli;

class Subject
{

    public $id;
    public $name;
    public $URL_book;
    public $form;

    /* @var $_db mysqli */
    private $_db;

    public function __construct($db)
    {
        $this->_db = $db;
    }

    public function findOne($id)
    {
        /* @var $this- >_db mysqli */
        $result = $this->_db->query("SELECT * FROM subjects WHERE id=".$id);

        if (!$result) {
            printf("Ошибка: %s\n", $this->_db->error);
            return false;
        }

        $row = $result->fetch_assoc();
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->URL_book = $row['URL_book'];
        $this->form = $row['form'];
    }

    public static function findAll($db, $filter = []): array
    {
        /* @var $db mysqli */

        $sql = "SELECT * FROM subjects";
        if (isset($filter['name'])) {
            $sql .= ' where name like "%' . $filter['name'] . '%"';
        }
        if (isset($filter['URL_book'])) {
            $sql .= ' and URL_book like "%' . $filter['URL_book'] . '%"';
        }
        if (isset($filter['form'])) {
            $sql .= ' and form like "%' . $filter['form'] . '%"';
        }
        $result = $db->query($sql);
        if (!$result) {
            printf("Ошибка: %s\n", $db->error);
            return false;
        }

        $row = $result->fetch_all(MYSQLI_ASSOC);
        $subjects = [];
        foreach ($row as $item) {
            $subject = new self($db);
            $subject->id = $item['id'];
            $subject->name = $item['name'];
            $subject->URL_book = $item['URL_book'];
            $subject->form = $item['form'];
            $subjects[] = $subject;
        }

        return $subjects;
    }

    public function loadFromGet($data)
    {
        if (isset($data['id'])) {
            $this->id = intval($data['id']);
        }
        if (isset($data['name'])) {
            $this->name = htmlspecialchars($data['name']);
        }
        if (isset($data['URL_book'])) {
            $this->URL_book = htmlspecialchars($data['URL_book']);
        }
        if (isset($data['form'])) {
            $this->form = intval($data['form']);
        }
    }

    public function save()
    {
        if ($this->id == null) {
            $result = $this->_db->prepare("INSERT INTO subjects VALUES (null, ?, ?, ?)");
        } else {
            $result = $this->_db->prepare("UPDATE subjects SET name=?,URL_book=?,form=? WHERE id =" . $this->id);
        }
        $result->bind_param("ssi", $this->name, $this->URL_book , $this->form);
        $result->execute();
        $result->close();
    }

    public function delete($id)
    {
        $result = $this->_db->query("DELETE FROM subjects WHERE id = " . $id);
    }
    public static function getSubjectList($db){
        $array=self::findAll($db);
        $subjectList=[];
        /* @var $item \classes\Subject*/
        foreach ($array as $item){
            $subjectList[$item->id]=$item->name;
        }
        return $subjectList;
    }

}