<?php

namespace classes;

class StudentsFilter
{
    const FILTER_LIST = ['name', 'surname', 'form'];

    public static function filter($filter)
    {
        $array = [];
        foreach (self::FILTER_LIST as $item) {
            if (isset($filter[$item])) {
                $array[$item] = $filter[$item];
            } else {
                $array[$item] = '';
            }
        }
        return $array;
    }
}