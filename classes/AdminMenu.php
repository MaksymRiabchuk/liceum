<?php


namespace classes;


class AdminMenu
{
    public $menuItems;

    public function __construct()
    {
        $this->menuItems[] = ['active' => 'students', 'url' => '/admin/students.php', 'name' => 'Список учнів'];
        $this->menuItems[] = ['active' => 'teachers', 'url' => '/admin/teachers.php', 'name' => 'Список вчителів'];
        $this->menuItems[] = ['active' => 'subjects', 'url' => '/admin/subjects.php', 'name' => 'Список предметів'];
        $this->menuItems[] = ['active' => 'marks', 'url' => '/admin/marks.php', 'name' => 'Список Оцінок'];
    }
    public static function OutputMenu($active)
    {
        $menu = new self();
        $st = '<div class="page-sidebar"><ul class="x-navigation">
        <li class="xn-logo"><a href="index.html">Joli Admin</a><a href="#" class="x-navigation-control"></a></li>
        <li class="xn-profile"><a href="#" class="profile-mini"><img src="assets/images/users/avatar.jpg" alt="John Doe"/></a>
        <div class="profile"><div class="profile-image"><img src="assets/images/users/avatar.jpg" alt="John Doe"/></div>
            <div class="profile-data"><div class="profile-data-name">John Doe</div><div class="profile-data-title">Web Developer/Designer</div></div>
            <div class="profile-controls"><a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
            <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a></div></div></li><li class="xn-title">Navigation</li>';
        foreach ($menu->menuItems as $item) {
            $classActive = ($item['active'] == $active) ? 'class="active"' : '';
            $menuSt = '<li ' . $classActive . '><a href="' . $item['url'] . '"><span class="fa fa-desktop"></span> <span class="xn-text">' . $item['name'] . '</span></a></li>';
            $st .= $menuSt;
        }

        $st .= '</ul></div>';

        echo $st;
    }

}