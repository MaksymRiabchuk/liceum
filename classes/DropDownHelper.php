<?php


namespace classes;


class DropDownHelper
{
    /**
     * @param $name
     * @param $list
     * @return string
     */
    public static function widget($name,$list){
        $html='<select name="'. $name .'" class="form-control">';
        foreach ($list as $key => $item){
            $html.='<option value="'. $key.'">'.$item.'</option>';
        }
        $html.='</select>';
    return $html;
    }
}