<?php


namespace classes;


class MainMenu
{
    public $menuItems;


    public function __construct()
    {
        $this->menuItems[] = ['active' => 'index', 'url' => 'index.php', 'name' => 'Головна'];
        $this->menuItems[] = ['active' => 'students', 'url' => 'students.php', 'name' => 'Список учнів'];
        $this->menuItems[] = ['active' => 'teachers', 'url' => 'teachers.php', 'name' => 'Список вчителів'];
        $this->menuItems[] = ['active' => 'subjects', 'url' => 'subjects.php', 'name' => 'Список предметів'];
        $this->menuItems[] = ['active' => 'marks', 'url' => 'marks.php', 'name' => 'Список оцінок'];
    }

    public static function OutputMenu($active)
    {
        $menu = new self();
        $st = '<header>
    <div class="logo">
        <div>
            <img src="/img/personal2.png" ; width="100%">
        </div>
        <div class="btn btn-danger" style="position: absolute;right: 10px;top: 10px">
            <a href="admin/students.php">Admin панель</a>
        </div>
    </div>
    <div class="main-menu"><ul class="nav nav-pills">';
        foreach ($menu->menuItems as $item) {
            $classActive = ($item['active'] == $active) ? 'class="active"' : '';
            $menuSt = '<li role="presentation" ' . $classActive . '><a href="' . $item['url'] . '">' . $item['name'] . '</a></li>';
            $st .= $menuSt;
        }

        $st .= '</ul></div></header>';

        echo $st;
    }
}