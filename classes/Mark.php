<?php


namespace classes;


use mysqli;

class Mark
{
    public $id;
    public $student_id;
    public $subject_id;
    public $teacher_id;
    public $mark;
    public $student_name;
    public $student_surname;
    public $teacher_name;
    public $subject_name;

    /* @var $_db mysqli */
    private $_db;

    public function __construct($db)
    {
        $this->_db = $db;
    }

    public function findOne($id)
    {
        /* @var $this- >_db mysqli */
        $result = $this->_db->query("SELECT * FROM marks WHERE id=" . $id);
        if (!$result) {
            printf("Ошибка: %s\n", $this->_db->error);
            return false;
        }

        $row = $result->fetch_assoc();
        $this->id = $row['id'];
        $this->student_id = $row['student_id'];
        $this->teacher_id = $row['teacher_id'];
        $this->subject_id = $row['subject_id'];
        $this->mark = $row['mark'];

    }

    public static function findAll($db, $filter = []): array
    {
        /* @var $db mysqli */

        $sql = "SELECT marks.*,students.`name` AS 'student_name',students.surname as 'student_surname',subjects.`name` as 'subject_name',teachers.`name` as 'teacher_name'
                FROM marks
                INNER JOIN students ON marks.student_id = students.id
                INNER JOIN subjects ON marks.subject_id = subjects.id
                INNER JOIN teachers ON marks.teacher_id = teachers.id";
        if (isset($filter['subject_name'])) {
            $sql .= ' where subjects.name like "%' . $filter['subject_name'] . '%"';
        }
        if (isset($filter['student_name'])) {
          $sql .= ' and students.name like "%' . $filter['student_name'] . '%"';
        }
        if (isset($filter['teacher_name'])) {
          $sql .= ' and teachers.name like "%' . $filter['teacher_name'] . '%"';
        }
        if (isset($filter['mark'])) {
            $sql .= ' and mark like "%' . $filter['mark'] . '%"';
        }
        $result = $db->query($sql);
        if (!$result) {
            printf("Ошибка: %s\n", $db->error);
            return false;
        }
        $row = $result->fetch_all(MYSQLI_ASSOC);
        $marks = [];
        foreach ($row as $item) {
            $mark = new self($db);
            $mark->id = $item['id'];
            $mark->student_id = $item['student_id'];
            $mark->teacher_id = $item['teacher_id'];
            $mark->subject_id = $item['subject_id'];
            $mark->mark = $item['mark'];
            $mark->student_surname = $item['student_surname'];
            $mark->student_name = $item['student_name'];
            $mark->teacher_name = $item['teacher_name'];
            $mark->subject_name = $item['subject_name'];

            $marks[] = $mark;
        }

        return $marks;
    }

    public function loadFromGet($data)
    {
        if (isset($data['id'])) {
            $this->id = intval($data['id']);
        }
        if (isset($data['student_id'])) {
            $this->student_id = intval($data['student_id']);
        }
        if (isset($data['subject_id'])) {
            $this->subject_id = intval($data['subject_id']);
        }
        if (isset($data['teacher_id'])) {
            $this->teacher_id = intval($data['teacher_id']);
        }
        if (isset($data['mark'])) {
            $this->mark = intval($data['mark']);
        }
    }

    public function save()
    {
        if ($this->id == null) {
            $result = $this->_db->prepare("INSERT INTO marks VALUES (null,?, ?, ?, ?)");
        } else {
            $result = $this->_db->prepare("UPDATE marks SET student_id=?, subject_id=?, teacher_id=?, mark=? WHERE id =" . $this->id);
        }
        $result->bind_param("iiii", $this->student_id, $this->subject_id,$this->teacher_id, $this->mark);
        $result->execute();
        $result->close();
    }

    public function delete($id)
    {
        $result = $this->_db->query("DELETE FROM marks WHERE id = " . $id);
    }

}