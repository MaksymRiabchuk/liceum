<?php


namespace classes;


use mysqli;

class Student
{
    public $id;
    public $name;
    public $surname;
    public $form;

    /* @var $_db mysqli */
    private $_db;

    public function __construct($db)
    {
        $this->_db = $db;
    }

    public function findOne($id)
    {
        /* @var $this- >_db mysqli */
        $result = $this->_db->query("SELECT * FROM students WHERE id=" . $id);

        if (!$result) {
            printf("Ошибка: %s\n", $this->_db->error);
            return false;
        }

        $row = $result->fetch_assoc();
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->surname = $row['surname'];
        $this->form = $row['form'];
    }

    public static function findAll($db, $filter = []): array
    {
        /* @var $db mysqli */

        $sql = "SELECT * FROM students";
        if (isset($filter['name'])) {
            $sql .= ' where name like "%' . $filter['name'] . '%"';
        }
        if (isset($filter['surname'])) {
            $sql .= ' and surname like "%' . $filter['surname'] . '%"';
        }
        if (isset($filter['form'])) {
            $sql .= ' and form like "%' . $filter['form'] . '%"';
        }
        $result = $db->query($sql);
        if (!$result) {
            printf("Ошибка: %s\n", $db->error);
            return false;
        }

        $row = $result->fetch_all(MYSQLI_ASSOC);
        $students = [];
        foreach ($row as $item) {
            $student = new self($db);
            $student->id = $item['id'];
            $student->name = $item['name'];
            $student->surname = $item['surname'];
            $student->form = $item['form'];
            $students[] = $student;
        }

        return $students;
    }

    public function loadFromGet($data)
    {
        if (isset($data['id'])) {
            $this->id = intval($data['id']);
        }
        if (isset($data['name'])) {
            $this->name = htmlspecialchars($data['name']);
        }
        if (isset($data['surname'])) {
            $this->surname = htmlspecialchars($data['surname']);
        }
        if (isset($data['form'])) {
            $this->form = intval($data['form']);
        }
    }

    public function save()
    {
        if ($this->id == null) {
            $result = $this->_db->prepare("INSERT INTO students VALUES (null,?, ?, ?)");
        } else {
            $result = $this->_db->prepare("UPDATE students SET name=?, surname=?, form=? WHERE id =" . $this->id);
        }
        $result->bind_param("ssi", $this->name, $this->surname, $this->form);
        $result->execute();
        $result->close();
    }

    public function delete($id)
    {
        $result = $this->_db->query("DELETE FROM students WHERE id = " . $id);
    }

    public static function getStudentList($db){
        $array=self::findAll($db);
        $studentList=[];
        /* @var $item \classes\Student*/
        foreach ($array as $item){
            $studentList[$item->id]=$item->name. ' ' . $item->surname;
        }
        return $studentList;
    }
}